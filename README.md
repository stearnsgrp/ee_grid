# **Greenland Iceberg Distribution (GrID) using machine learning and cloud computing**

---

The project extracts and integrates with GEE using geemap. A machine learning model is trained using manually classified icebergs in Sentinel-1 imagery. The model outputs a classification map of water, icebergs, and sea-ice. The classified imagery is now labeled into 4-connected components and every connected component is given a new label. 


Area of all connected components is estimated by a product of number of pixel in a cluster (Np) and area of a pixel (Area).

* Area of iceberg = (Np) x (Area) 

The coordinates of all icebergs are calculated by getting the centroid of iceberg.

* Coordinates (X, Y)

Both the above components will now be added to a dataframe. The icebergs can then be plotted on a map and can be used to calculate the frequency size distribution.

The model segments icebergs that are seen in Sentinel-1 SAR imagery in varying swaths of 10m, 25m, and 40m pixel spacing.** 


### **Labels**

* Water (W): 0

* Iceberg (I): 1

* SeaIce (S): 2

---

###### *From May 2017 - IW and EW have consistent HH, HV polarization available around Greenland. The Model uses three bands: HH, HV, (HHxHV)*


###### *Prior to May 2017: IW and EW have inconsistent HH, HV polarization coverage. So we will be using Grey level Co-occurence Matrix (GLCM) along with the Sigma0 band with Sentinel-1 imagery. The GLCM bands provide textural information that is used by the model to identify icebergs.*


